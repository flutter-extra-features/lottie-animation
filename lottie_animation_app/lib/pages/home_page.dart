import 'package:flutter/material.dart';
import 'package:lottie/lottie.dart';

class HomePage extends StatefulWidget {
  const HomePage({super.key});

  @override
  State<HomePage> createState() => _HomePageState();
}

class _HomePageState extends State<HomePage>
    with SingleTickerProviderStateMixin {
  late final AnimationController _controller;

  @override
  void initState() {
    super.initState();
    _controller = AnimationController(
      vsync: this,
      duration: Durations.extralong4,
    );
  }

  @override
  void dispose() {
    _controller.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: _buildUI(),
      floatingActionButton: FloatingActionButton(
        onPressed: () {
          var ticker = _controller
              .forward(); // celeration lottie comes only once, when only forward used
          ticker.whenComplete(() {
            _controller
                .reset(); // when icon gets clicked, celebration lottie runs
          });
        },
        child: const Icon(
          Icons.play_arrow,
        ),
      ),
    );
  }

  Widget _buildUI() {
    return Stack(
      children: [
        Center(
          child: Lottie.asset(
            "assets/animations/Animation - 1719036592540.json",
            repeat: true, // default is true
            width: 250,
            height: 250,
            // reverse: true,
          ),
        ),
        Lottie.asset(
          "assets/animations/Animation - 1719036658506.json",
          controller: _controller,
          width: MediaQuery.sizeOf(context).height,
          height: MediaQuery.sizeOf(context).width,
          fit: BoxFit.cover,
          repeat: false,
        ),
      ],
    );
  }
}
